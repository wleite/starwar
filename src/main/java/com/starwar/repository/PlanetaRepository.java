package com.starwar.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.starwar.model.Planeta;

@RepositoryRestResource(collectionResourceRel = "planeta", path = "planeta")
public interface PlanetaRepository extends MongoRepository<Planeta,Integer>{

	@Query("{ 'nome' : ?0 }")
	public Optional<Planeta> findName(String nome);

}
