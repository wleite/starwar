package com.starwar.controller;

import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.starwar.model.Planeta;
import com.starwar.service.GenericService;

@RestController
@CrossOrigin
@RequestMapping("api")
public class PlanetaController {
	
	@Autowired	
	private GenericService<Planeta> service;
	
	//-------------------Retrieve All Planeta--------------------------------------------------------
	@GetMapping(value="/planeta")
	public ResponseEntity<Collection<Planeta>> getAll(){
		Collection<Planeta> items = service.getAll();
		return new ResponseEntity<>(items,HttpStatus.OK);
	}	
	
	//-------------------Retrieve Single Planeta--------------------------------------------------------
	@GetMapping(value = "/planeta/{id}")
	public ResponseEntity<Planeta> getObject(@PathVariable("id") Integer id) {
		Optional<Planeta> item = service.getId(id);
		if (!item.isPresent()) {			
			return new ResponseEntity<Planeta>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Planeta>(item.get(), HttpStatus.OK);
	}
	
	//-------------------Retrieve Single Planeta--------------------------------------------------------
	@GetMapping(value = "/planeta/{nome}")
	public ResponseEntity<Planeta> getObject(@PathVariable("nome") String nome) {
		Optional<Planeta> item = service.getName(nome);
		if (!item.isPresent()) {			
			return new ResponseEntity<Planeta>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Planeta>(item.get(), HttpStatus.OK);
	}
	
	//-------------------Create Planeta--------------------------------------------------------
	@PostMapping(value="/planeta")
	public ResponseEntity<Planeta> create(@RequestBody Planeta planeta){	
		service.save(planeta);		
		return new ResponseEntity<Planeta>(HttpStatus.CREATED);
	}	
	
	//------------------- Update Planeta --------------------------------------------------------
	@PutMapping(value="/planeta")
	public ResponseEntity<Planeta> update(@RequestBody Planeta planeta){
		Planeta item = null;
		item = service.save(planeta);		
		return new ResponseEntity<Planeta>(item,HttpStatus.ACCEPTED);
	}	

	//------------------- Delete a Planeta --------------------------------------------------------	     
	@DeleteMapping(value="/planeta/{id}")
	public ResponseEntity<Collection<Planeta>> delete(@PathVariable Integer id){
		Optional<Planeta> item = service.getId(id);
		if(!item.isPresent()){
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		service.remove(item.get());
		return new ResponseEntity<>(HttpStatus.OK);
	}	

}
