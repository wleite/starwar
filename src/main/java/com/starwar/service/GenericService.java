package com.starwar.service;

import java.util.Collection;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.starwar.model.Planeta;


public abstract interface GenericService<T> {
	
	public T save(T entity);

	public Page<T> getAll(Pageable pageable);	
	
	public Collection<T> getAll();
	
	public void remove(T entity);
	
	public Optional<T> getId(Integer id);

	public Optional<Planeta> getName(String nome);

}
