package com.starwar.serviceImp;

import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.starwar.model.Planeta;
import com.starwar.repository.PlanetaRepository;
import com.starwar.service.GenericService;

@Service
@Transactional
public class PlanetaServiceImp implements GenericService<Planeta>{

	@Autowired
	private PlanetaRepository planetaRepository;
	
	@Override
	public Planeta save(Planeta planeta) {
		return planetaRepository.save(planeta);
	}

	@Override
	public Page<Planeta> getAll(Pageable pageable) {		
		return planetaRepository.findAll(pageable);
	}

	@Override
	public Collection<Planeta> getAll() {
		return planetaRepository.findAll();
	}

	@Override
	public void remove(Planeta planeta) {
		planetaRepository.delete(planeta);
	}

	@Override
	public Optional<Planeta> getId(Integer id) {
		return planetaRepository.findById(id);
	}

	@Override
	public Optional<Planeta> getName(String nome) {		
		return planetaRepository.findName(nome);
	}

}
